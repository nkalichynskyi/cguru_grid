import React from 'react'
import DuckImage from '../assets/Duck.jpg'
import './HomeView.scss'
import Griddle from '../../../components/Grid/index';



const columnMetadata = [
	{
    "columnName": "name",
    "order": 9,
    "locked": false,
    "visible": true,
    "displayName": "Employee Name"
  },
  {
    "columnName": "city",
    "order": 8,
    "locked": false,
    "visible": true
  },
  {
    "columnName": "state",
    "order": 7,
    "locked": false,
    "visible": true
  },
  {
    "columnName": "country",
    "order": 6,
    "locked": false,
    "visible": true
  },
  {
    "columnName": "company",
    "order": 5,
    "locked": false,
    "visible": true
  },
  {
    "columnName": "favoriteNumber",
    "order":  4,
    "locked": false,
    "visible": true,
    "displayName": "Favorite Number",
    "sortable": false
  }
]

export const HomeView = () => (
  <div>
		Grid example
    <Griddle url='someURL' columnMetadata={columnMetadata} />
  </div>
);

export default HomeView;
