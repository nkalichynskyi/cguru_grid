import React from 'react'
import Header from '../../components/Header'
import './CoreLayout.scss'
import '../../styles/core.scss'

export const CoreLayout = ({ children }) => (
  <div className='container'>
		<div className='row'>
			<div className='col-md-1'>
				{/*nav*/}
				navigation
			</div>
			<div className='col-md-2'>
				{/*pages*/}
				pages tree
			</div>
			<div className='col-md-9'>
				{/*header*/}
				{children}
			</div>
		</div>
    {/*<Header />*/}
  </div>
)

CoreLayout.propTypes = {
  children : React.PropTypes.element.isRequired
}

export default CoreLayout
