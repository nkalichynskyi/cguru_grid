import fetch from 'isomorphic-fetch'
import axios from 'axios'
import Immutable from 'immutable'

// ------------------------------------
// Constants
// ------------------------------------
export const REQUEST_DATA = 'REQUEST_DATA'
export const RECEIVE_DATA = 'RECEIVE_DATA'
export const RECEIVE_DATA_ERROR = 'RECEIVE_DATA_ERROR'


// ------------------------------------
// Actions
// ------------------------------------

export function fetchError(error) {
  return dispatch => {
    dispatch({ error, type: RECEIVE_DATA_ERROR });
  };
}

function fetchSuccess(response) {
  return dispatch => {
    dispatch({ response, type: RECEIVE_DATA });
  };
}

function dataRequest(gridData) {
  return dispatch => {
    dispatch({ gridData, type: REQUEST_DATA });
  };
}

const data = [
{
  "id": 0,
  "name": "Mayer Leonard",
  "city": "Kapowsin",
  "state": "Hawaii",
  "country": "United Kingdom",
  "company": "Ovolo",
  "favoriteNumber": 7
  },
  {
    "id": 1,
    "name": "Koch Becker",
    "city": "Johnsonburg",
    "state": "New Jersey",
    "country": "Madagascar",
    "company": "Eventage",
    "favoriteNumber": 2
  },
	{
    "id": 2,
    "name": "Koch Becker",
    "city": "Johnsonburg",
    "state": "New Jersey",
    "country": "Madagascar",
    "company": "Eventage",
    "favoriteNumber": 2
  },
	{
    "id": 3,
    "name": "Koch Becker",
    "city": "Johnsonburg",
    "state": "New Jersey",
    "country": "Madagascar",
    "company": "Eventage",
    "favoriteNumber": 2
  },
	{
    "id": 4,
    "name": "Koch Becker",
    "city": "Johnsonburg",
    "state": "New Jersey",
    "country": "Madagascar",
    "company": "Eventage",
    "favoriteNumber": 2
  },
	{
    "id": 5,
    "name": "Koch Becker",
    "city": "Johnsonburg",
    "state": "New Jersey",
    "country": "Madagascar",
    "company": "Eventage",
    "favoriteNumber": 2
  },
	{
    "id": 6,
    "name": "Koch Becker",
    "city": "Johnsonburg",
    "state": "New Jersey",
    "country": "Madagascar",
    "company": "Eventage",
    "favoriteNumber": 2
  },
];

export function getData(gridData) {
	dataRequest(gridData);
  return dispatch =>
		dispatch(fetchSuccess(data));
		/*axios.get('/api/data', {params:{gridData}})
    .then(response => {
      if (response.status >= 200 && response.status < 300) {
        console.log(response);
        dispatch(fetchSuccess(response));
      } else {
        const error = new Error(response.statusText);
        error.response = response;
        dispatch(fetchError(error));
        throw error;
      }
    })
    .catch(error => { console.log('request failed', error); });*/
}

// ------------------------------------
// Reducer
// ------------------------------------
let initialState = new Immutable.Map()
export default function gridData(state = initialState, action) {
  switch (action.type) {
    case REQUEST_DATA:
      return state.merge({
        requestStatus: "loading",
				data: []
      });
    case RECEIVE_DATA:
      return state.merge({
        error: null,
				data: action.response,
        requestStatus: "succeeded",
      });
    case RECEIVE_DATA_ERROR:
      return state.merge({
        error: action.error,
        requestStatus: "failed",
        data: []
      });
      break;
    default:
      return state;
  }
}
