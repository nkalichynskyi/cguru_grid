import React from 'react';
import Griddle from '../../griddle/griddle.jsx';
//import Griddle from 'griddle-react';
//import GridActions from 'Actions/grid_actions';
import _ from 'lodash';
//import {Constants as GlobalConstants} from '../../settings';
//import ToolsBar from './toolsbar.jsx';

const Loading = ({loadingText = "Loading..."}) => (
	<div className="loading">{this.props.loadingText}</div>
);

Loading.propTypes = {
	loadingText: React.PropTypes.string
}

export default class GridWrapper extends React.Component {
	constructor(props) {
    super(props);
  }

	componentWillMount() {
		this.props.getData()
	}

	componentWillUnmount() {
		//TODO clear storew
		//awdawd
	}

	render() {
		const columns = _.map(this.props.columnMetadata, el => el.columnName)
		const data = this.props.gridData.get('data') ? this.props.gridData.get('data').toJS() : []

		return(
			<Griddle columns={columns} columnMetadata={this.props.columnMetadata}
					 useExternal={false} useFixedLayout={false} results={data}
			/>
		)
	}
}

React.createClass({

	getInitialState(){

		const data = Store.getData().items;
		const total_count = Store.getData().total_count;
		return {
			maxPage: Math.ceil(total_count / (this.state ? this.state.pageSize : this.props.pageSize || 5)) || 1,
			page: this.state ? this.state.page : 0,
			data: data,
			allDataHandler : false,
			filter : this.state && this.state.filter ? this.state.filter : {},
			pageSize: this.state ? this.state.pageSize : this.props.pageSize || 5,
			dataState: Store.getState(),
			isLoading: data.isEmpty() && Store.getState() === GlobalConstants.StateTypes.loading
		}
	},

	componentWillMount(){
		GridActions.setPage({
			utils: dataComponentWrapper({
				Utils: this.props.Utils,
				fetchMethodName: this.props.fetchMethodName
			}, 'Utils'),
			page: 0,
			per_page: this.state.pageSize
		}, this.props.additionalParams);
	},

	componentDidMount: function () {
		this.props.Store.addChangeListener(this._onChange);
	},

	componentWillUnmount: function () {
		this.props.Store.removeChangeListener(this._onChange);
	},

	shouldComponentUpdate(nextProps, nextState){
		if (nextState.dataState === "update_succeed" ||
			nextState.dataState === GlobalConstants.StateTypes.post_succeed ||
			nextState.dataState === GlobalConstants.StateTypes.put_succeed ||
			nextState.dataState === GlobalConstants.StateTypes.delete_succeed) {

			this.fetchData({pageIndex : this.state.page});
			return false;
		}
		if (this.state.allDataHandler) {
			//not sure about this 'mapper'. If u're sure - delete comment
			let data = nextState.data.toJS();
			let mapper = nextProps.dataMapper || this.props.dataMapper;
			if (mapper) {
				data = _.map(data, mapper);
			}

			this.state.allDataHandler(data);
			return false;
		}
		return true;
	},
	formAddParams(filter){
		filter = filter || this.state.filter;
		let addParams = this.props.additionalParams || {};
		if (!_.isEmpty(filter)) {
			_.assign(addParams, filter)
		}
		return addParams;
	},
	fetchData(params){
		var pageIndex = params.pageIndex || 0,
			pageSize = params.pageSize || this.state.pageSize,
			addParams = params.addParams || this.formAddParams();

		GridActions.setPage({
			utils: dataComponentWrapper({
				Utils: this.props.Utils,
				fetchMethodName: this.props.fetchMethodName
			}, 'Utils'),
			page: pageIndex,
			per_page: pageSize
		}, addParams);
	},

	fetchAllData(dataHandler){
		let addParams = this.formAddParams();
		_.assign(addParams, {allData : true})
		this.setState({allDataHandler: dataHandler});
		this.fetchData({addParams : addParams});
	},

	fetchFilteredData(filter){
		let addParams = this.formAddParams(filter);
		this.setState({filter : filter, page: 0, isLoading: true});
		this.fetchData({addParams : addParams});
	},

	_onChange(){
		this.setState(this.getInitialState());
	},

	setPage(index){
		//This should interact with the data source to get the page at the given index
		this.setState({page: index, isLoading: true});
		this.fetchData({pageIndex : index});
	},

	setPageSize(size){
		this.setState({pageSize: size});
		this.fetchData({pageSize : size});
	},

	onRowClick(row, e){
		if (this.props.onRowClick) {
			this.setState({selectedRowId: row.props.data._id});
			this.props.onRowClick(row, e);
		}
	},

	render(){
		let columns = _.map(this.props.columnMetadata, el => el.columnName);
		let grid = this;
		var rowMetadata = {
			"bodyCssClassName": function (rowData) {
				let classes = "";
				if (grid.props.rowStyle)
					classes += grid.props.rowStyle(rowData);
				if (!grid.props.selectable)
					classes += " default-row";
				if (rowData._id === grid.state.selectedRowId)
					classes += " default-row selected-row";
				return classes += " default-row selectable";
			}
		};

		let data = this.state.data.toJS();
		if (this.props.dataMapper) {
			data = _.map(data, this.props.dataMapper);
		}
		let _griddle = (
			<div>
				{this.props.showToolbar
					?<ToolsBar columns={this.props.toolsbarColumns} fetchAllData={this.fetchAllData}/>
				:<br/>
				}

				<Griddle columns={columns} columnMetadata={this.props.columnMetadata}
						 useExternal={true} externalSetPage={this.setPage} rowMetadata={rowMetadata}
						 externalMaxPage={this.state.maxPage} onRowClick={this.onRowClick}
						 externalCurrentPage={this.state.page} useFixedLayout={false}
						 results={data} resultsPerPage={this.state.pageSize}
						 pageSize={this.state.pageSize}
						 externalChangeSort={()=>{}} externalSetFilter={()=>{}} externalSetPageSize={this.setPageSize}
						 customRowComponent={this.props.customRowComponent}
						 useCustomRowComponent={this.props.useCustomRowComponent}
						 expandable={this.props.expandable} customSubGrid={this.props.customSubGrid}
						 externalLoadingComponent={Loading} externalIsLoading={this.state.isLoading}
				/>
			</div>
		);
		//!!!!!!!
		//customFilterComponent should call 'handleChange' and pass there 'filterObj'
		//to start fetching filtered data
		return (
			<div>
				{this.props.customFilterComponent
					?
					(<div>
						<div className="col-md-2">
							<this.props.customFilterComponent handleChange = {this.fetchFilteredData} />
						</div>
						<div className="col-md-10">
							{_griddle}
						</div>
					</div>)
					:
					_griddle
				}
			</div>
		)
	}
});
