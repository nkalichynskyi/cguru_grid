import { connect } from 'react-redux'
import { getData } from '../modules/gridModules'

/*  This is a container component. Notice it does not contain any JSX,
    nor does it import React. This component is **only** responsible for
    wiring in the actions and state necessary to render a presentational
    component - in this case, the counter:   */

import GridWrapper from '../components/GridWrapper'

/*  Object of action creators (can also be function that returns object).
    Keys will be passed as props to presentational components. Here we are
    implementing our wrapper around increment; the component doesn't care   */

const mapDispatchToProps = {
  getData
}

const mapStateToProps = (state) => {
		return {
				gridData : state.get('gridData')
		}
}

export default connect(mapStateToProps, mapDispatchToProps)(GridWrapper)
