/*
 See License / Disclaimer https://raw.githubusercontent.com/DynamicTyped/Griddle/master/LICENSE
 */
var React = require('react');

var GridRowContainer = React.createClass({
	getDefaultProps: function () {
		return {
			"useGriddleStyles": true,
			"useGriddleIcons": true,
			"isSubGriddle": false,
			"columnSettings": null,
			"rowSettings": null,
			"paddingHeight": null,
			"rowHeight": null,
			"parentRowCollapsedClassName": "parent-row",
			"parentRowExpandedClassName": "parent-row expanded",
			"parentRowCollapsedComponent": "▶",
			"parentRowExpandedComponent": "▼",
			"onRowClick": null,
			"multipleSelectionSettings": null
		};
	},
	getInitialState: function () {
		return {
			"data": {},
			"showChildren": false
		}
	},
	componentWillReceiveProps: function () {
		this.setShowChildren(false);
	},
	toggleChildren: function () {
		this.setShowChildren(this.state.showChildren === false);
	},
	setShowChildren: function (visible) {
		this.setState({
			showChildren: visible
		});
	},
	verifyProps: function () {
		if (this.props.columnSettings === null) {
			console.error("gridRowContainer: The columnSettings prop is null and it shouldn't be");
		}
	},
	render: function () {
		this.verifyProps();
		var that = this;
		if (typeof this.props.data === "undefined") {
			return (<tbody></tbody>);
		}
		var arr = [];

		var columns = this.props.columnSettings.getColumns();

		arr.push(
			<this.props.rowSettings.rowComponent
				useGriddleStyles={this.props.useGriddleStyles}
				isSubGriddle={this.props.isSubGriddle}
				data={this.props.rowSettings.isCustom ? _.pick(this.props.data, columns) : this.props.data}
				rowData={this.props.rowSettings.isCustom ? this.props.data : null }
				columnSettings={this.props.columnSettings}
				rowSettings={this.props.rowSettings}
				hasChildren={that.props.hasChildren}
				toggleChildren={that.toggleChildren}
				expandable={this.props.expandable}
				showChildren={that.state.showChildren}
				key={that.props.uniqueId + '_base_row'}
				useGriddleIcons={that.props.useGriddleIcons}
				parentRowExpandedClassName={this.props.parentRowExpandedClassName}
				parentRowCollapsedClassName={this.props.parentRowCollapsedClassName}
				parentRowExpandedComponent={this.props.parentRowExpandedComponent}
				parentRowCollapsedComponent={this.props.parentRowCollapsedComponent}
				paddingHeight={that.props.paddingHeight}
				rowHeight={that.props.rowHeight}
				onRowClick={that.props.onRowClick}
				multipleSelectionSettings={this.props.multipleSelectionSettings}
			/>
		);

		var children = null;

		if (that.state.showChildren) {
			children = (
				<tr key="expand">
					<td style={{backgroundColor: "#FFF"}} colSpan={this.props.columnSettings.columnMetadata.length}>
						<this.props.customSubGrid data={this.props.data}></this.props.customSubGrid>
					</td>
				</tr>);
		}

		return that.props.expandable === false ? arr[0] :
			<tbody>{that.state.showChildren ? arr.concat(children) : arr}</tbody>
	}
});

module.exports = GridRowContainer;
