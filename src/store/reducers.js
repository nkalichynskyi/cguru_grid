import { combineReducers } from 'redux-immutable'
import locationReducer from './location'
import GridReducers from '../components/Grid/modules/gridModules';

export const makeRootReducer = (asyncReducers) => {
  return combineReducers({
    location: locationReducer,
		gridData: GridReducers,
    ...asyncReducers
  })
}

export const injectReducer = (store, { key, reducer }) => {
  if (Object.hasOwnProperty.call(store.asyncReducers, key)) return

  store.asyncReducers[key] = reducer
  store.replaceReducer(makeRootReducer(store.asyncReducers))
}

export default makeRootReducer
